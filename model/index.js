module.exports ={
    adminModule : require('./admin'),
    userModule : require('./user'),
    categoryModule : require('./category'),
    productModule : require('./product'),
    shipperModule: require('./shipper'),
    paymentModule: require('./payment'),
    orderModule : require('./order'),
    inventoryModule : require('./inventory'),
    shipperAvailabilityModule : require('./shipper_availability'),

}


